window.addEventListener('load', function() {
	//stran nalozena
	var gumb = document.getElementById("prijavniGumb");
	
	var prijava = function(){
		var ime = document.querySelector("input[id='uporabnisko_ime']").value;
		var uporabnik = document.getElementById("uporabnik");
		
		uporabnik.innerHTML = ime; 
		
		var pokrivalo = document.querySelector(".pokrivalo");
		pokrivalo.style.visibility = "hidden";
	}
	gumb.addEventListener('click',prijava);

	var gumb2 = document.getElementById("dodajGumb");
	var dodajOpomnik = function(){
		var naziv = document.querySelector("input[id='naziv_opomnika']").value;
		var cas = document.querySelector("input[id='cas_opomnika']").value;
		
		//reset
		document.querySelector("input[id='cas_opomnika']").value = "";
		document.querySelector("input[id='naziv_opomnika']").value = "";
		
		var opomniki = document.getElementById("opomniki");
		opomniki.innerHTML += "<div class='opomnik senca rob'>" +
								"<div class='naziv_opomnika'>" + naziv + "</div>" +
								"<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div>" +
							 "</div>";
		
	}
	gumb2.addEventListener('click',dodajOpomnik);
	

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			
			var naziv_opomnika = document.querySelector(".naziv_opomnika");
			if(cas == 0){
				alert("Opomnik!\n\nZadolžitev '" + naziv_opomnika.innerHTML + "' je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			else{
				cas = cas - 1;
				casovnik.innerHTML = cas;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);
});
